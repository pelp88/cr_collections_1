package ru.coderiders.collections.task1;

import java.util.ArrayList;
import java.util.List;

public class TestLaunch {
    public static void main(String[] args) {
        List<Object> test = new ArrayList<>(List.of("afoiwqjfqi", "eifgjewoi", 1, 1, 2, 3, "afoiwqjfqi"));
        System.out.println("Before: " + test);
        System.out.println("After: " + Dedup.deduplicate(test));
    }
}
