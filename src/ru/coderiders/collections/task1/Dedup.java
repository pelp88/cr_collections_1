package ru.coderiders.collections.task1;

import java.util.Collection;
import java.util.HashSet;

public class Dedup {
    static public Collection<Object> deduplicate(Collection<Object> collection) {
        HashSet<Object> set = new HashSet<>(collection);
        collection.clear();
        collection.addAll(set);
        return collection;
    }
}
