package ru.coderiders.collections.task2;

import java.util.ArrayList;
import java.util.List;

public class TestLaunch {
    public static void main(String[] args) {
        List<Long> timelist1 = new ArrayList<>();
        List<Long> timelist2 = new ArrayList<>();
        Picker picker = new Picker();
        picker.addElements();

        //First iteration
        long current1 = System.currentTimeMillis();
        picker.getArraylistRandomElems();
        long current2 = System.currentTimeMillis();
        timelist1.add(current2 - current1);
        System.out.println("ArrayList first iteration: " + (current2 - current1));

        current1 = System.currentTimeMillis();
        picker.getLinkedlistRandomElems();
        current2 = System.currentTimeMillis();
        timelist2.add(current2 - current1);
        System.out.println("LinkedList first iteration: " + (current2 - current1));

        //Second iteration
        current1 = System.currentTimeMillis();
        picker.getArraylistRandomElems();
        current2 = System.currentTimeMillis();
        timelist1.add(current2 - current1);
        System.out.println("ArrayList second iteration: " + (current2 - current1));

        current1 = System.currentTimeMillis();
        picker.getLinkedlistRandomElems();
        current2 = System.currentTimeMillis();
        timelist2.add(current2 - current1);
        System.out.println("LinkedList second iteration: " + (current2 - current1));

        //Third iteration
        current1 = System.currentTimeMillis();
        picker.getArraylistRandomElems();
        current2 = System.currentTimeMillis();
        timelist1.add(current2 - current1);
        System.out.println("ArrayList third iteration: " + (current2 - current1));

        current1 = System.currentTimeMillis();
        picker.getLinkedlistRandomElems();
        current2 = System.currentTimeMillis();
        timelist2.add(current2 - current1);
        System.out.println("LinkedList third iteration: " + (current2 - current1));

        //Fourth iteration
        current1 = System.currentTimeMillis();
        picker.getArraylistRandomElems();
        current2 = System.currentTimeMillis();
        timelist1.add(current2 - current1);
        System.out.println("ArrayList fourth iteration: " + (current2 - current1));

        current1 = System.currentTimeMillis();
        picker.getLinkedlistRandomElems();
        current2 = System.currentTimeMillis();
        timelist2.add(current2 - current1);
        System.out.println("LinkedList fourth iteration: " + (current2 - current1));

        System.out.println("Average for arraylist: " + timelist1.stream().mapToDouble(a -> a).average().orElse(0.0));
        System.out.println("Average for linkedlist: " + timelist2.stream().mapToDouble(a -> a).average().orElse(0.0));
    }
}
