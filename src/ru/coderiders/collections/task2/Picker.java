package ru.coderiders.collections.task2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Picker {
    // ArrayList быстрее, чем LinkedList, при взятии произвольного элемента.
    // Время доступа для ArrayList: O (1). Время доступа для LinkedList: O (n).
    List<Integer> arraylist = new ArrayList<>();
    List<Integer> linkedlist = new LinkedList<>();

    public void addElements() {
        this.arraylist = IntStream.rangeClosed(0, 1000000)
                .boxed().collect(Collectors.toList());
        this.linkedlist = IntStream.rangeClosed(0, 1000000)
                .boxed().collect(Collectors.toList());

        Collections.shuffle(this.arraylist);
        Collections.shuffle(this.linkedlist);
    }

    public void getArraylistRandomElems() {
        Random rand = new Random();

        for (int i = 0; i < this.arraylist.size(); i++) {
            int randomIndex = rand.nextInt(this.arraylist.size());
            Integer randomElement = this.arraylist.get(randomIndex);
        }
    }

    public void getLinkedlistRandomElems() {
        Random rand = new Random();

        for (int i = 0; i < this.linkedlist.size(); i++) {
            int randomIndex = rand.nextInt(this.linkedlist.size());
            Integer randomElement = this.linkedlist.get(randomIndex);
        }
    }
}
